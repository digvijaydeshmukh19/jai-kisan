import {Injectable} from "@angular/core";
import {Pagination} from "../pagination/pagination-model";
@Injectable()
export class PagerService {
  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10): Pagination {
    const totalPages = Math.ceil(totalItems / pageSize);

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    let  pages : number[] = new Array(0);
    for(let i = startPage; i < endPage ; i++) {
      pages.push(i);
    }

    let obj : Pagination = new Pagination();
    obj.startPage = startPage;
    obj.endPage = endPage;
    obj.currentPage = currentPage;
    obj.endIndex = endIndex;
    obj.pages = pages;
    obj.pageSize = pageSize;
    obj.totalItems = totalItems;
    obj.totalPages = totalPages;
    obj.startIndex = startIndex;
    return obj;
  }
}
