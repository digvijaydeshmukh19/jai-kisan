import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HttpModule } from '@angular/http';
import { UserService } from './user/user.service';
import { PagerService } from './service/pager.service';
import { SortPipe } from './custom-pipes/sort.pipe';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    SortPipe,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [UserService,PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
