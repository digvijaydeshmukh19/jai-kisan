import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';
import { Pagination } from '../pagination/pagination-model';
import { PagerService } from '../service/pager.service';
import { SortEnum } from '../custom-pipes/sort.enum';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  sortOrder: SortEnum = SortEnum.FIRST_NAME;
  searchText: string;

  user:User[]=new Array(0);
  data:any[];
  private paginationRecords = 6;
  pager: Pagination = new Pagination();
  constructor(private _UserService:UserService, private pagerService: PagerService) { }

  ngOnInit() {
    this._UserService.getUser().subscribe(
      data => {
        this.data = data;
        this.setPage(1);
      },
      error => {
          console.error(error);
      });
    
  }
  getSortEnum() {
    return SortEnum;
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.data.length, page, this.paginationRecords);
    this.user = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

}
