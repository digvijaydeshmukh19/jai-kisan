import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {  Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from './user';
import { BaseService } from '../service/baseService.service';
@Injectable()
export class UserService extends BaseService {

  constructor(private http:Http) {
    super();
   }

  getUser():Observable<User[]>{
    let url:string='/assets/user.json';
    return this.http.get(url).map(this.extractData);
  }
}
