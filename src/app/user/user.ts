export class User{
    constructor(
        public id:string,
        public  firstName:string,
        public lastName:string,
        public email:string,
        public gender:string,
        public companyName:string,
        public jobTitle:string,
        public university:string,
        public skill:string
    ){}
}