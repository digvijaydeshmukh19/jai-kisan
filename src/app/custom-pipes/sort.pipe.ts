import {Pipe, PipeTransform} from '@angular/core';



import { SortEnum } from './sort.enum';
import { User } from '../user/user';

@Pipe({
    name: 'sort'
  })
  export class SortPipe implements PipeTransform{
  
      transform(array: Array<User>, args: SortEnum): Array<User> {
        if (typeof args === 'undefined') {
          return array;
        }
        if(!array){
          return array;
        }
        array.sort((a: User, b: User) => {
          switch (args) {
            case SortEnum.FIRST_NAME:
              if (  a.firstName.localeCompare(b.firstName) === -1) {
                return -1;
              }
              if (a.firstName.localeCompare(b.firstName) === 1) {
                return 1;
              }
              return 0;
              case SortEnum.UNIVERSITY:
              if (  a.university.localeCompare(b.university) === -1) {
                return -1;
              }
              if (a.university.localeCompare(b.university) === 1) {
                return 1;
              }
              return 0;
              case SortEnum.COMPANY_NAME:
              if (  a.companyName.localeCompare(b.companyName) === -1) {
                return -1;
              }
              if (a.companyName.localeCompare(b.companyName) === 1) {
                return 1;
              }
              return 0;
           
          }
        });
        return array;
      }
  }
  